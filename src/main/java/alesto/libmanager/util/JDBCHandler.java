package alesto.libmanager.util;

import alesto.libmanager.Operation;

import java.sql.*;

/**
 * Created on 04.10.2016.
 */
public class JDBCHandler {
    private static final String URL = "jdbc:mysql://localhost:3306/librarian";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    static {

    }

    private ResultSet handleRequest(Operation operation, String query) {
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
        PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
