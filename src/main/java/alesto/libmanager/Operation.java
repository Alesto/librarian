package alesto.libmanager;

/**
 * Created on 04.10.2016.
 */
public enum Operation {
    ADD,
    EDIT,
    GET,
    GET_ALL
}
