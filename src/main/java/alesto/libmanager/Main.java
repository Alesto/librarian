package alesto.libmanager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created on 03.10.2016.
 */
public class Main {
    public static void main(String[] args) {
        startLibrarian();
    }

    private static void startLibrarian() {
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Welcome to library!");
            String input;
            while(!(input = reader.readLine()).equals("exit")) {
                processInput(input);
            }
            System.out.println("Thanks for using our terminal.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void processInput(String input) {
        String command = input.split(" ")[0];
        switch(command) {
            case "add":
                System.out.println("dick");
                break;
            case "edit":

                break;
            case "remove":

                break;
            case "all":
                System.out.println("here all books");
                break;
            default:
                System.out.println("unknown command");
        }
    }
}
