package alesto.libmanager.dao;

import alesto.libmanager.domain.Book;

import java.util.List;

/**
 * Created on 03.10.2016.
 */
public interface LibraryDAO {

    /**
     * Returns all books which name has
     * @param query
     * @return
     */
    List<Book> getBook(String query);

    List<Book> getBooks();

    Book addBook(String name, String[] authors);

    Book editBookName(String oldName, String newName);

    Book removeBook(String name);


}
