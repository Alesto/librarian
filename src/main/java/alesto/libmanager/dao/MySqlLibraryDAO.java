package alesto.libmanager.dao;

import alesto.libmanager.domain.Book;

import java.util.List;

/**
 * Created on 03.10.2016.
 */
public class MySqlLibraryDAO implements LibraryDAO {
    @Override
    public List<Book> getBook(String query) {
        return null;
    }

    @Override
    public List<Book> getBooks() {
        return null;
    }

    @Override
    public Book addBook(String name, String[] authors) {
        return null;
    }

    @Override
    public Book editBookName(String oldName, String newName) {
        return null;
    }

    @Override
    public Book removeBook(String name) {
        return null;
    }
}
